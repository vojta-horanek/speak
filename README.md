Ukol na UPG

Melo by fungovat na Linuxu, na windows jsem nezkousel a negarantuji kompatibilitu.
Demo zde: [https://www.youtube.com/watch?v=kquetBSypcc](https://www.youtube.com/watch?v=kquetBSypcc)

## TODO (to co by to jeste chtelo, kdyby jsem to mel nejak vydat)
1) Nehardcodovat IP serveru
2) Zjistovat na lokalni siti servery pomoci IP broadcastu
3) Podpora IPv6
4) Lepsi interface (bud graficke Qt nebo konzolove ncurses)
5) Historie zprav
6) Celkovy code cleanup, moc se mi to nelibi, ale funguje to
7) Zkontrolovat memory leaky, urcite tam nekde jsou
8) Kontorolovat duplicitu jmen
9) Vylepsit cteni zprav (momentalne to asi nezvladne nic nad 1024 znaku)
10) Pridat indikator, ze uzivatel pise
11) Pouzit nejaky normalni protokol (MQTT?), protoze to stejnak vzdy bude lepsi nez neco custom
