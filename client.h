//
// Created by vojta on 3/11/20.
//

#ifndef SPEAK_CLIENT_H
#define SPEAK_CLIENT_H

void client_start();
void send_message(const std::string& string);
void client_exit();
void client_welcome(const std::string& name);
void parse_input(const std::string& basicString);
void* client_receive(void *arg);

#endif //SPEAK_CLIENT_H
