//
// Created by vojta on 3/11/20.
//

#include "definitions.h"

#include "client.h"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <csignal>

int sock = 0;
volatile sig_atomic_t stop_client;

void client_show_interface();

void inthand_client(int signum) {
    stop_client = 1;
}

void client_start() {
    D("Running in client mode");

    struct sockaddr_in serv_addr{};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        E("Could not create socket");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(SERVER_PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        E("Invalid address/ Address not supported");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        E("Connection Failed");
        exit(EXIT_FAILURE);
    }

    signal(SIGINT, inthand_client);

    std::string name;
    std::cout << "Enter your name: ";
    std::cin >> name;
    std::cin.ignore();

    client_welcome(name);

    pthread_t receiveT;
    if (pthread_create(&receiveT, nullptr,
                       client_receive, nullptr) != 0) {
        E("Could not create a new thread for receiving from server");
        exit(EXIT_FAILURE);
    }

    client_show_interface();

    std::string msg;
    while (!stop_client) {
        getline(std::cin, msg);
        if (std::cin.eof()) break;
        if (msg.empty()) {
            continue;
        }
        parse_input(msg);
    }

    client_exit();
}

void client_show_interface() {

}

void parse_input(const std::string &input) {
    if (input.starts_with('/')) {
        if (input == "/exit") {
            client_exit();
            exit(EXIT_SUCCESS);
        }
    } else {
        send_message(input);
    }
}

void send_message(const std::string &string) {
    const size_t size = C_SIZE * string.length() + 2;
    char *data = static_cast<char *>(malloc(size));
    data[0] = MSG_DATA_TXT;
    std::strcpy(data + 1, string.c_str());
    write(sock, data, size);
}

void client_exit() {
    char data[] = {MSG_DATA_BYE, 0};
    write(sock, data, sizeof(data));
}

void client_welcome(const std::string &name) {
    const size_t size = C_SIZE * name.length() + 2;
    char *data = static_cast<char *>(malloc(size));
    data[0] = MSG_DATA_HEY;
    std::strcpy(data + C_SIZE, name.c_str());
    write(sock, data, size);
}

void *client_receive(void *arg) {

    char *buffer = static_cast<char *>(malloc(sizeof(char) * 1024));
    if (buffer == nullptr) {
        E("Could not allocate memory for buffer for a new client");
        pthread_exit(nullptr);
    }

    while (!stop_client) {

        ssize_t readBytes = read(sock, buffer, 1024);
        if (readBytes == 0) break;
        char *currentData = buffer;
        int nextCommandPos = 0;
        bool parse = true;
        while (parse) {
            std::cout << (currentData + 1);

            while (buffer[nextCommandPos++] != 0) {
                if (nextCommandPos < readBytes) {
                    parse = false;
                    break;
                }
            }
            currentData = buffer + nextCommandPos;
        }
    }

    close(sock);
    free(buffer);

    pthread_exit(nullptr);
}
