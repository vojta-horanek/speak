//
// Created by vojta on 3/11/20.
//

#include "definitions.h"
#include "server.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <vector>
#include <mutex>
#include <csignal>
#include <condition_variable>
#include <cstring>

std::vector<int> clients;
std::mutex clientsLock;

std::mutex sendM;
std::condition_variable cv;
bool ready = false;

std::string msgToSend;
int sender = 0;

volatile sig_atomic_t stop_server;

pthread_t sendT, listenT;

void inthand_server(int signum){
    stop_server = 1;
    pthread_kill(sendT, SIGINT);
    pthread_kill(listenT, SIGINT);
    // Toto je spatne, ale uz jsem se do tech threadu nejak zamotal a funguje to
    exit(EXIT_SUCCESS);
}

void server_start() {
    D("Running in server mode");

    int fd;

    signal(SIGINT, inthand_server);

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == 0) {
        E("Could not create socket");
        exit(EXIT_FAILURE);
    }

    int opt = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        E("Could not set socket options");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in address{};

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(SERVER_PORT);

    if (bind(fd, reinterpret_cast<const sockaddr *>(&address), sizeof(address)) < 0) {
        E("Could not attach socket to port " << SERVER_PORT);
        exit(EXIT_FAILURE);
    }

    if (listen(fd, MAX_CONNECTIONS) < 0) {
        E("Could not listen on socket");
        exit(EXIT_FAILURE);
    }

    int *arg = static_cast<int *>(malloc(sizeof(*arg)));
    if (arg == nullptr) {
        E("Could not allocate memory for thread argument");
        exit(EXIT_FAILURE);
    }
    *arg = fd;
    if (pthread_create(&listenT, nullptr,
                       server_listen, arg) != 0) {
        E("Could not create a new thread for listening to connections");
        exit(EXIT_FAILURE);
    }

    if (pthread_create(&sendT, nullptr,
                       server_send, nullptr) != 0) {
        E("Could not create a new thread for sending to clients");
        exit(EXIT_FAILURE);
    }

    pthread_join(listenT, nullptr);
    pthread_join(sendT, nullptr);
}

void *server_send(void *arg) {

    while (!stop_server) {
        std::unique_lock<std::mutex> lk(sendM);
        cv.wait(lk, [] { return ready; });

        std::string msg = msgToSend;
        int _sender = sender;
        std::cout << msg;

        for (auto& client : clients) {
            if (client == _sender) continue; // Don't send message to sender
            const size_t size = C_SIZE * msg.length() + 2;
            char *data = static_cast<char *>(malloc(size));
            data[0] = MSG_DATA_TXT;
            std::strcpy(data + 1, msg.c_str());
            write(client, data, size);
        }

        ready = false;
        lk.unlock();
        cv.notify_one();
    }
    pthread_exit(nullptr);
}

void *server_listen_on_client(void *client) {
    int client_fd = *((int *) client);
    free(client);
    std::string client_name;

    char *buffer = static_cast<char *>(malloc(sizeof(char) * 1024));
    if (buffer == nullptr) {
        E("Could not allocate memory for buffer for a new client");
        pthread_exit(nullptr);
    }

    bool clientConnected = true;
    while (clientConnected) {
        ssize_t readBytes = read(client_fd, buffer, 1024);
        if (readBytes == 0) break;
        char *currentData = buffer;
        int nextCommandPos = 0;
        bool parse = true;
        while (parse) {
            switch (currentData[0]) {
                case MSG_DATA_HEY:
                    client_name = currentData + 1;
                    I("[INFO] " << client_name << " connected");
                    break;
                case MSG_DATA_BYE:
                    I("[INFO] " << client_name << " disconnected");
                    clientConnected = false;
                    break;
                case MSG_DATA_TXT:
                    msgToSend = client_name + "> " + (currentData + 1) + "\n";
                    sender = client_fd;
                    ready = true;
                    cv.notify_one();
                    break;
                default:
                    E("Unspecified data indicator");
                    break;
            }
            while (buffer[nextCommandPos++] != 0) {
                if (nextCommandPos < readBytes) {
                    parse = false;
                    break;
                }
            }
            currentData = buffer + nextCommandPos;
        }
    }

    close(client_fd);

    free(buffer);

    clientsLock.lock();
    for (int i = 0; i < clients.size(); i++) {
        if (clients[i] == client_fd) {
            clients.erase(clients.begin() + i);
            break;
        }
    }
    clientsLock.unlock();

    pthread_exit(nullptr);
}

void *server_listen(void *_fd) {
    int fd = *((int *) _fd);
    free(_fd);
    struct sockaddr_in address{};
    int addrlen = sizeof(address);
    while (!stop_server) {
        int new_fd = accept(fd, reinterpret_cast<sockaddr *>(&address), (socklen_t *) &addrlen);
        if (new_fd < 0) {
            E("Could not accept new connection from " << address.sin_addr.s_addr);
        } else {
            pthread_t pthread;
            int *arg = static_cast<int *>(malloc(sizeof(*arg)));
            if (arg == nullptr) {
                E("Could not allocate memory for thread argument");
                pthread_exit(nullptr);
            }
            *arg = new_fd;
            if (pthread_create(&pthread, nullptr,
                               server_listen_on_client, arg) != 0) {
                E("Could not create a new thread for client");
            } else {
                clientsLock.lock();
                clients.push_back(new_fd);
                clientsLock.unlock();
            }
        }
    }

    pthread_exit(nullptr);
}