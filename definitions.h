//
// Created by vojta on 3/11/20.
//

#ifndef SPEAK_DEFINITIONS_H
#define SPEAK_DEFINITIONS_H

#include <iostream>

#ifdef DEBUG
#define D(x) std::cerr << x << std::endl;
#else
#define D(x) do{} while(0)
#endif

#define E(x) std::cerr << x << std::endl;

#define I(x) std::cout << x << std::endl;

#define SERVER_PORT 2558
#define MAX_CONNECTIONS 5

#define MSG_DATA_TXT 'T'
#define MSG_DATA_HEY 'H'
#define MSG_DATA_BYE 'B'

#define C_SIZE sizeof(char)

#endif //SPEAK_DEFINITIONS_H
