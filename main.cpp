#include <cstring>

#include "definitions.h"
#include "client.h"
#include "server.h"

int main(int argc, char *argv[]) {

    if (argc < 2) {
        E("No mode specified, assuming client");
        client_start();
    } else {
        if (strcmp(argv[1], "client") == 0) {
            client_start();
        } else if (strcmp(argv[1], "server") == 0) {
            server_start();
        } else {
            E(argv[1] << ": not a valid mode");
            exit(1);
        }
    }

    return 0;
}
