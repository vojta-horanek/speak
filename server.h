//
// Created by vojta on 3/11/20.
//

#ifndef SPEAK_SERVER_H
#define SPEAK_SERVER_H

void server_start();
void *server_listen(void *_fd);
void *server_send(void *arg);

#endif //SPEAK_SERVER_H
